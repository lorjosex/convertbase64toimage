﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CrystalDecisions.ReportSource;
using CrystalDecisions.Shared;
using CrystalDecisions.CrystalReports;
using CrystalDecisions.Windows;
using CrystalDecisions.Windows.Forms;
using CrystalDecisions.CrystalReports.Engine;
using System.Data.SqlClient;
using System.Diagnostics;

namespace ConvertBase64ToImage
{
    public partial class frmConvertBase64ToImage : Form
    {
        
        public frmConvertBase64ToImage()
        {
            InitializeComponent();
        }

        private void Button1_Click(object sender, EventArgs e)
        {
            OpenFileDialog open = new OpenFileDialog();
            open.ShowDialog();
            txtRutaArchivo.Text = open.FileName;
            pictureBox1.Image = Image.FromFile(txtRutaArchivo.Text);
        }

        private void Button2_Click(object sender, EventArgs e)
        {
            try
            {
            string StringBase64 = txtStringBase64.Text;
            byte[] archivoBytes = Convert.FromBase64String(StringBase64);
                if (txtStringBase64.Text == ""){ MessageBox.Show("El Campo CODE BASE64 esta vacío", "Base64", MessageBoxButtons.OK, MessageBoxIcon.Information); txtStringBase64.Focus(); return; }
                if (txtRutaArchivo.Text == "")
                { MessageBox.Show("Debe elegir la ruta de acceso\npor ejemplo: C:\\File\\Archivo.jpg", "Ruta de acceso", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txtRutaArchivo.Focus();
                    return;
                }
            System.IO.File.WriteAllBytes(txtRutaArchivo.Text, archivoBytes);
            pictureBox1.Image = Image.FromFile(txtRutaArchivo.Text);

            }
            catch (Exception)
            {

                throw;
            }
        }

        private void Button3_Click(object sender, EventArgs e)
        {
            string ruta = txtRutaArchivo.Text;
            byte[] archivoBytes = System.IO.File.ReadAllBytes(txtRutaArchivo.Text);
            string StringBase64 = Convert.ToBase64String(archivoBytes);
            txtStringBase64.Text = StringBase64;
        }

        private void BtnimprimirReporte_Click(object sender, EventArgs e)
        {
            ReportDocument objReport = new ReportDocument();
            string NombreRte = "FacturaContado.rpt";
            //ParameterFields Parametro = new ParameterFields();
            Cursor.Current = Cursors.WaitCursor;
            string NombreReporte = @"C:\ExFE\ReportePruebas\" + NombreRte;
            string PDFBase64;
            objReport.Load(NombreReporte);
            objReport.SetParameterValue("@fac", 1049);
            objReport.SetParameterValue("@pre", "SETT");
            objReport.SetParameterValue("@NitFacturador", "1");
            objReport.ExportToDisk(ExportFormatType.PortableDocFormat, @"C:\ExFE\ReportePruebas\FacturaContado.Pdf");
            byte[] pdfBytes = File.ReadAllBytes("C:\\ReportesPruebas\\Pruebas.Pdf");
            PDFBase64 = Convert.ToBase64String(pdfBytes);
            MessageBox.Show("PDF Generado Correctamente");
            string pdfPath = Path.Combine(@"C:\ExFE\ReportePruebas\", "FacturaContado.Pdf");
            Process.Start(pdfPath);
            Cursor.Current = Cursors.Default;
        }

        private void Button4_Click(object sender, EventArgs e)
        {
            //SqlConnection conexion = new SqlConnection("Server=ALBERTO-PC\\ALBERTO2014;Database=Quiron;Trusted_Connection=True;");
            //SqlCommand command = new SqlCommand("SELECT QrImage FROM EncabezadoFactura_Electronica WHERE IdEncabezadoFactura = 21", conexion);
            //SqlDataAdapter sqlData = new SqlDataAdapter(command);
            //DataSet dataSet = new DataSet("EncabezadoFactura_Electronica");

            //byte[] imagen = new byte[0];
            //sqlData.Fill(dataSet, "EncabezadoFactura_Electronica");
            //DataRow myRow = dataSet.Tables["EncabezadoFactura_Electronica"].Rows[0];
            //imagen = (byte[])myRow["QrImage"];
            //MemoryStream memoryStream = new MemoryStream(imagen);


            ////-------
            //ParameterField parametro = new ParameterField();
            //ReportDocument objReport = new ReportDocument();
            //string NombreRte = "ReportePruebas1.rpt";
            ////ParameterFields Parametro = new ParameterFields();
            //Cursor.Current = Cursors.WaitCursor;
            //string NombreReporte = "C:\\ReportesPruebas\\" + NombreRte;
            //string PDFBase64;
            //objReport.Load(NombreReporte);
            //objReport.SetParameterValue("@fac", 43495);
            //objReport.SetParameterValue("@pre", "SETT");
            //objReport.SetParameterValue("@NitFacturador", "1");
            //objReport.SetParameterValue("@Image", imagen);
            //objReport.ExportToDisk(ExportFormatType.PortableDocFormat, "C:\\ReportesPruebas\\Pruebas.Pdf");
            //byte[] pdfBytes = File.ReadAllBytes("C:\\ReportesPruebas\\Pruebas.Pdf");
            //PDFBase64 = Convert.ToBase64String(pdfBytes);
            //MessageBox.Show("PDF Generado Correctamente");
            //Cursor.Current = Cursors.Default;
            ////-------

            //pictureBox1.Image = Image.FromStream(memoryStream);

        }

        private void BtnClear_Click(object sender, EventArgs e)
        {
            txtStringBase64.Text = "";
            pictureBox1.Image = null;
            txtRutaArchivo.Text = "";
        }

        private void Panel1_Paint(object sender, PaintEventArgs e)
        {

        }
    }
}
